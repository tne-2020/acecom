# Recupération de la version
FIND_PACKAGE(Git)

function(git_version project)

    EXECUTE_PROCESS (
            COMMAND ${GIT_EXECUTABLE} rev-list --tags --max-count=1
            WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
            OUTPUT_VARIABLE GIT_TAG_COMMIT
            OUTPUT_STRIP_TRAILING_WHITESPACE
    )
    EXECUTE_PROCESS (
            COMMAND ${GIT_EXECUTABLE} describe --tag ${GIT_TAG_COMMIT}
            WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
            OUTPUT_VARIABLE GIT_DESCRIBE_VERSION
            OUTPUT_STRIP_TRAILING_WHITESPACE
    )
    EXECUTE_PROCESS (
            COMMAND ${GIT_EXECUTABLE} rev-list ${GIT_TAG_COMMIT}..HEAD --count
            WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
            OUTPUT_VARIABLE VERSION_GIT_COUNT
            OUTPUT_STRIP_TRAILING_WHITESPACE
    )

    if(NOT "${GIT_DESCRIBE_VERSION}")
        set(GIT_DESCRIBE_VERSION "v0.0.0")
    endif()

    # On parse la version
    STRING(REGEX REPLACE "v([0-9]*)\\.([0-9]*)\\.([0-9]*)"
            "\\1.\\2." VERSION "${GIT_DESCRIBE_VERSION}" )
    STRING(CONCAT VERSION "${VERSION}${VERSION_PATCH}" )
    STRING(REGEX REPLACE "v([0-9]*)\\.([0-9]*)\\.([0-9]*)"
            "\\1" VERSION_MAJOR "${GIT_DESCRIBE_VERSION}")
    STRING(REGEX REPLACE "v([0-9]*)\\.([0-9]*)\\.([0-9]*)"
            "\\2" VERSION_MINOR "${GIT_DESCRIBE_VERSION}")
    STRING(REGEX REPLACE "v([0-9]*)\\.([0-9]*)\\.([0-9]*)"
            "\\3" VERSION_PATCH "${GIT_DESCRIBE_VERSION}")
    math(EXPR VERSION_PATCH "${VERSION_PATCH} + ${VERSION_GIT_COUNT}")

    STRING(CONCAT VERSION "${VERSION}${VERSION_PATCH}" )
    MESSAGE( STATUS "- " ${PROJECT_NAME} " v" ${VERSION} )

    # Ajout en tant que define
    add_definitions(-DVERSION="${VERSION}")
    add_definitions(-DVERSION_MAJOR=${VERSION_MAJOR})
    add_definitions(-DVERSION_MINOR=${VERSION_MINOR})
    add_definitions(-DVERSION_PATCH=${VERSION_PATCH})

    if(${ARGC} EQUAL 1)
        set(${ARGV0} ${VERSION} PARENT_SCOPE)
    endif()

    set_target_properties(${project} PROPERTIES VERSION "${VERSION}")
endfunction()

function(iscompatible VAR app lib)
    get_target_property(VERSION_APP ${app} VERSION)
    get_target_property(VERSION_LIB ${lib} VERSION)

    STRING(REGEX REPLACE "v([0-9]*)\\.([0-9]*)\\.([0-9]*)"
            "\\1" VERSION_APP_MAJOR "${VERSION_APP}")
    STRING(REGEX REPLACE "v([0-9]*)\\.([0-9]*)\\.([0-9]*)"
            "\\2" VERSION_APP_MINOR "${VERSION_APP}")

    STRING(REGEX REPLACE "v([0-9]*)\\.([0-9]*)\\.([0-9]*)"
            "\\1" VERSION_LIB_MAJOR "${VERSION_LIB}")
    STRING(REGEX REPLACE "v([0-9]*)\\.([0-9]*)\\.([0-9]*)"
            "\\2" VERSION_LIB_MINOR "${VERSION_LIB}")

    if(NOT VERSION_APP_MAJOR EQUAL VERSION_LIB_MAJOR)
        set(${VAR} false PARENT_SCOPE)
    elseif(NOT VERSION_LIB_MINOR EQUAL VERSION_LIB_MINOR)
        set(${VAR} false PARENT_SCOPE)
    else ()
        set(${VAR} true PARENT_SCOPE)
    endif()
endfunction()
