# Recupération de la version
cmake_minimum_required(VERSION 3.12)

FIND_PACKAGE(Git)

function(git_download cots)
    set(URL_GIT "https://git.deming.fr/binaries")

    set(DIR_TMP "${CMAKE_BINARY_DIR}/tmp")
    set(ZIP_NAME "${DIR_TMP}/${cots}.zip")

    set(DIR_THIRDPARTY "${CMAKE_BINARY_DIR}/thirdparty")
    set(FINAL_DEST "${DIR_THIRDPARTY}/${cots}")

    set(DOWNLOADED_${cots}_DIR ${FINAL_DEST} CACHE INTERNAL "Dir of cots" FORCE)

    if(EXISTS "${FINAL_DEST}")
        message(STATUS "Git Dowload: ${cots} already downloaded")

        file(REMOVE ${ZIP_NAME})
        file(REMOVE ${DIR_TMP})
    else()
        file(MAKE_DIRECTORY ${DIR_THIRDPARTY})

        if(NOT EXISTS "${ZIP_NAME}")
                message("Git Dowload: Downloading ${cots}...")
                file(MAKE_DIRECTORY ${DIR_TMP})
                file(DOWNLOAD ${URL_GIT}/${cots}/-/archive/master/${cots}-master.zip ${ZIP_NAME})
        endif()

        message("Git Dowload: Extracting ${cots}...")
        execute_process(
                COMMAND ${CMAKE_COMMAND} -E tar t ${ZIP_NAME}
                OUTPUT_VARIABLE tarfiles
                OUTPUT_STRIP_TRAILING_WHITESPACE
        )

        string(REPLACE "\n" ";" tarfiles ${tarfiles})
        LIST(FILTER tarfiles INCLUDE REGEX "^[^/]*/?$")

        execute_process(
                COMMAND ${CMAKE_COMMAND} -E tar zxf ${ZIP_NAME}
                WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/tmp/
                OUTPUT_VARIABLE tarfiles_out
                ERROR_VARIABLE tarfiles_err
        )

        list(LENGTH tarfiles tarfileslen)
        if(tarfileslen EQUAL 1)
            execute_process(
                    COMMAND ${CMAKE_COMMAND} -E rename ${DIR_TMP}/${tarfiles} ${FINAL_DEST}
            )
            file(REMOVE ${ZIP_NAME})
            file(REMOVE ${DIR_TMP})
            message("Git Dowload: Downloading ${cots} - done")
        else()
            message( FATAL_ERROR "Your download contains more than one folder or is empty" )
        endif()

    endif()

endfunction()


