#include "server.hxx"

#include "ability.hxx"
#include "log.hxx"
#include "session.hxx"
#include <boost/bind.hpp>

using boost::asio::ip::tcp;

namespace io {

Server::Server(boost::asio::io_service &io_service, const tcp::endpoint &endpoint)
    : _acceptor(io_service, endpoint)
    , _socket(io_service)
    , _ability(std::make_shared<io::AbilityServer>())
{
    do_accept();
}

void Server::do_accept()
{
    _acceptor.async_accept(_socket, [this](boost::system::error_code ec) {
        if (!ec) {
            auto session = std::make_shared<io::Session>(std::move(_socket), _ability);
            if (on_accept(session)) {
                session->onStop(std::bind(&Server::on_disconnect, this, session));
                session->start();

                logging::trace("Session started");
            }
            else {
                logging::debug("Client refused");
            }
        }

        do_accept();
    });
}

} // namespace io
