#pragma once

#include <map>
#include <string>

namespace io::type {
enum {
    MASTER_CLIENT_CONNEXION,
    MASTER_DISCONNECT,
    MASTER_PING,
    MASTER_PONG,
    MASTER_SERVER,
    MASTER_SERVER_INFO,
    TCHAT_CHANNEL_EVENT,
    TCHAT_CHANNEL_JOIN,
    TCHAT_CHANNEL_LEAVE,
    TCHAT_CHANNEL_MESSAGE,
    TCHAT_CHANNEL_PRIVATE_MESSAGE,
    TCHAT_CHANNEL_SEND_MESSAGE,
    TCHAT_CHANNEL_SEND_PRIVATE_MESSAGE,
    TCHAT_LOGIN,
    TCHAT_CHANNEL_INFO,
};

}

namespace io {

using Item = std::pair<std::string_view, std::uint8_t>;
constexpr Item types[] = {
    { "io.master.ClientConnexion", type::MASTER_CLIENT_CONNEXION },
    { "io.master.Disconnect", type::MASTER_DISCONNECT },
    { "io.master.Ping", type::MASTER_PING },
    { "io.master.Pong", type::MASTER_PONG },
    { "io.master.Server", type::MASTER_SERVER },
    { "io.master.ServerInfo", type::MASTER_SERVER_INFO },
    { "io.tchat.ChannelEvent", type::TCHAT_CHANNEL_EVENT },
    { "io.tchat.ChannelInfo", type::TCHAT_CHANNEL_INFO },
    { "io.tchat.ChannelJoin", type::TCHAT_CHANNEL_JOIN },
    { "io.tchat.ChannelLeave", type::TCHAT_CHANNEL_LEAVE },
    { "io.tchat.ChannelMessage", type::TCHAT_CHANNEL_MESSAGE },
    { "io.tchat.ChannelPrivateMessage", type::TCHAT_CHANNEL_PRIVATE_MESSAGE },
    { "io.tchat.ChannelSendMessage", type::TCHAT_CHANNEL_SEND_MESSAGE },
    { "io.tchat.ChannelSendPrivateMessage", type::TCHAT_CHANNEL_SEND_PRIVATE_MESSAGE },
    { "io.tchat.Login", type::TCHAT_LOGIN },
};

constexpr auto map_size = sizeof types / sizeof types[0];

static constexpr std::uint8_t findValue(std::string_view key, int range = map_size)
{
    return (range == 0)
        ? throw "Key not present"
        : (types[range - 1].first == key) ? types[range - 1].second : findValue(key, range - 1);
}

static constexpr std::string_view findKey(std::uint8_t value, int range = map_size)
{
    return (range == 0)
        ? throw "Value not present"
        : (types[range - 1].second == value) ? types[range - 1].first : findKey(value, range - 1);
}

static constexpr bool contains(std::string_view key, int range = map_size)
{
    return (range == 0) ? false : (types[range - 1].first == key) ? true : contains(key, range - 1);
}

#define IO_CHECK(key) static_assert(io::contains(key), "Key not registered")

} // namespace io
