#pragma once

#include "ability.hxx"
#include <boost/asio.hpp>

namespace io {
class Session;
} // namespace io

namespace io {

class Server {
protected:
    using SessionPtr = std::shared_ptr<Session>;

public:
    Server(boost::asio::io_service &io_service, const boost::asio::ip::tcp::endpoint &endpoint);

protected:
    template <typename Proto>
    void addAbility(const std::function<void(SessionPtr, std::shared_ptr<Proto>)> &callback);

    template <typename C, typename Proto>
    void addAbility(void (C::*callback)(SessionPtr, std::shared_ptr<Proto>));

    virtual bool on_accept(SessionPtr session) = 0;
    virtual void on_disconnect(SessionPtr session) = 0;

private:
    void do_accept();

private:
    boost::asio::ip::tcp::acceptor _acceptor;
    boost::asio::ip::tcp::socket _socket;

    std::shared_ptr<io::AbilityServer> _ability;
};

template <typename Proto>
void Server::addAbility(const std::function<void(SessionPtr, std::shared_ptr<Proto>)> &callback)
{
    _ability->addCallback<Proto>(callback);
}

template <typename C, typename Proto>
void Server::addAbility(void (C::*callback)(SessionPtr, std::shared_ptr<Proto>))
{
    _ability->addCallback<Proto>(
        std::bind(callback, static_cast<C *>(this), std::placeholders::_1, std::placeholders::_2));
}

} // namespace io
