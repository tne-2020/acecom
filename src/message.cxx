#include "message.hxx"

#include "ability.hxx"
#include "log.hxx"
#include <google/protobuf/message.h>

namespace io {

Message::Message()
{
    static_assert(HEADER_SIZE < BUFFER_SIZE);
}

Message::Message(std::shared_ptr<Ability> ability)
    : _ability(ability)
{
    static_assert(HEADER_SIZE < BUFFER_SIZE);
}

uint8_t *Message::headerBuffer()
{
    return _buffer.data();
}

uint8_t *Message::contentBuffer()
{
    decodeHeader();
    return _buffer.data() + HEADER_SIZE;
}

std::size_t Message::contentSize()
{
    decodeHeader();
    return static_cast<std::size_t>(_size);
}

bool Message::pack(std::shared_ptr<google::protobuf::Message> message)
{
    if (!message) {
        logging::warn("Trying to pack a null message");
        return false;
    }

    _type = findValue(message->GetDescriptor()->full_name());

    _size = message->ByteSizeLong();
    if (BUFFER_SIZE - HEADER_SIZE < _size) {
        throw std::runtime_error("Message size is too big for buffer " + std::to_string(_size) + "/"
            + std::to_string(BUFFER_SIZE - HEADER_SIZE));
    }

    encodeHeader();

    if (_type != io::type::MASTER_PING && _type != io::type::MASTER_PONG) {
        logging::trace("Pack with type={} and size={}", io::findKey(_type), _size);
        logging::trace("Pack {}", show_hex<Buffer>(_buffer.begin(), _buffer.begin() + HEADER_SIZE));
    }

    return message->SerializeToArray(&_buffer[HEADER_SIZE], _size);
}

bool Message::unpack(const std::shared_ptr<Session> session)
{
    if (_type != io::type::MASTER_PING && _type != io::type::MASTER_PONG) {
        logging::trace("Unpack with type={} and size={}", io::findKey(_type), _size);
        logging::trace("Unpack {}",
            show_hex<Buffer>(_buffer.begin(), _buffer.begin() + HEADER_SIZE));
    }

    std::shared_ptr<google::protobuf::Message> message = _ability->make(_type);

    if (message) {
        if (message->ParseFromArray(&_buffer[HEADER_SIZE], _size)) {
            if (message) {
                logging::trace("Session ({}): Receive {}\n{}",
                    (long long)session.get(),
                    message->GetDescriptor()->full_name(),
                    message->DebugString());
            }

            _ability->call(_type, session, message);
            return true;
        }
        else {
            logging::critical("Fail to parse message of type {}", _type);
        }
    }
    else {
        logging::critical("Fail to build message of type {}", io::findKey(_type));
    }

    return false;
}

void Message::decodeHeader()
{
    _size = 0;
    for (std::size_t i = 0; i < HEADER_SIZE_CONTENT; ++i) {
        _size += static_cast<int>(_buffer[i]) << ((HEADER_SIZE_CONTENT - i - 1) * 8);
    }

    _type = 0;
    for (std::size_t i = 0; i < HEADER_SIZE_TYPE; ++i) {
        _type += static_cast<int>(_buffer[HEADER_SIZE_CONTENT + i])
            << ((HEADER_SIZE_TYPE - i - 1) * 8);
    }
}

void Message::encodeHeader()
{
    for (std::size_t i = 0; i < HEADER_SIZE_CONTENT; ++i) {
        _buffer[HEADER_SIZE_CONTENT - i - 1] = static_cast<std::uint8_t>((_size >> i * 8) & 0xFF);
    }

    for (std::size_t i = 0; i < HEADER_SIZE_TYPE; ++i) {
        _buffer[HEADER_SIZE_CONTENT + HEADER_SIZE_TYPE - i - 1]
            = static_cast<std::uint8_t>((_type >> i * 8) & 0xFF);
    }
}

} // namespace io
