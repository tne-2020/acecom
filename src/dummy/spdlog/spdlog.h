#pragma once

#include <cstdio>

namespace spdlog {

template <typename... Args>
constexpr void trace(const char *, Args...)
{
}

template <typename... Args>
constexpr void debug(const char *, Args...)
{
}

template <typename... Args>
constexpr void info(const char *, Args...)
{
}

template <typename... Args>
constexpr void warn(const char *, Args...)
{
}

template <typename... Args>
constexpr void error(const char *, Args...)
{
}

template <typename... Args>
constexpr void critical(const char *, Args...)
{
}

} // namespace spdlog
