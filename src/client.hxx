#pragma once

#include "ability.hxx"
#include "types.hxx"
#include <boost/asio/deadline_timer.hpp>
#include <boost/asio/io_context.hpp>
#include <boost/asio/ip/tcp.hpp>

#include <queue>

#include <list>
#include <memory>
#include <string>
#include <thread>

namespace io {
class Ability;
class Session;
} // namespace io
namespace google::protobuf {
class Message;
}

namespace io {

class Client {
protected:
    using SessionPtr = std::shared_ptr<Session>;

public:
    Client();
    virtual ~Client();

    void start(const std::string &ip, std::int16_t port);
    void stop(bool reconnect = false);

    void deliver(std::shared_ptr<google::protobuf::Message> &&message);

    template <typename Proto>
    void deliver(std::shared_ptr<Proto> message)
    {
        deliver(std::static_pointer_cast<google::protobuf::Message>(message));
    }

protected:
    template <typename Proto>
    void addAbility(const std::function<void(std::shared_ptr<Proto>)> &callback);

    template <typename C, typename Proto>
    void addAbility(void (C::*callback)(std::shared_ptr<Proto>));

    virtual void onConnect();
    virtual void onDisconnect();

private:
    void doConnect(const boost::asio::ip::tcp::resolver::results_type &endpoints);
    void doDisconnect(const boost::asio::ip::tcp::resolver::results_type &endpoints);

private:
    boost::asio::io_context _ioContext;
    boost::asio::ip::tcp::resolver _resolver;
    boost::asio::ip::tcp::socket _socket;

    std::shared_ptr<AbilityClient> _ability;
    std::shared_ptr<Session> _session;

    std::unique_ptr<std::thread> _thread;

    std::queue<std::shared_ptr<google::protobuf::Message>> _toBeDelivered;

    bool _stop;
    boost::asio::deadline_timer _timerReconnect;
};

template <typename Proto>
void Client::addAbility(const std::function<void(std::shared_ptr<Proto>)> &callback)
{
    _ability->addCallback<Proto>(callback);
}

template <typename C, typename Proto>
void Client::addAbility(void (C::*callback)(std::shared_ptr<Proto>))
{
    _ability->addCallback<Proto>(
        std::bind(callback, static_cast<C *>(this), std::placeholders::_1));
}

} // namespace io
