#pragma once

#include "types.hxx"

#include <functional>
#include <list>
#include <map>
#include <memory>

namespace google::protobuf {
class Message;
}

namespace io {
class Session;
}

namespace io {

class Ability {
public:
    virtual std::shared_ptr<google::protobuf::Message> make(std::uint8_t type) = 0;
    virtual void call(std::uint8_t type,
        std::shared_ptr<Session> session,
        std::shared_ptr<google::protobuf::Message> &message)
        = 0;
};

class AbilityServer : public Ability {
public:
    class Func {
    public:
        Func() = default;
        virtual ~Func() = default;
        virtual void call(std::shared_ptr<Session>, std::shared_ptr<google::protobuf::Message>) = 0;
    };

    template <typename Proto>
    class FuncSpec : public Func {
    public:
        FuncSpec(const std::function<void(std::shared_ptr<Session>, std::shared_ptr<Proto>)> &call)
            : _call(call)
        {
        }

        void call(std::shared_ptr<Session> session,
            std::shared_ptr<google::protobuf::Message> message) override
        {
            _call(session, std::static_pointer_cast<Proto>(message));
        }

    private:
        std::function<void(std::shared_ptr<Session>, std::shared_ptr<Proto>)> _call;
    };

public:
    AbilityServer();

    std::shared_ptr<google::protobuf::Message> make(std::uint8_t type) override;
    void call(std::uint8_t type,
        std::shared_ptr<Session> session,
        std::shared_ptr<google::protobuf::Message> &message) override;

    template <typename Proto>
    void addCallback(
        const std::function<void(std::shared_ptr<Session>, std::shared_ptr<Proto>)> &callback)
    {
        const std::string typeId = Proto::GetDescriptor()->full_name();

        const std::uint8_t type = findValue(typeId);
        _messageFactory[type] = []() { return std::make_shared<Proto>(); };
        _callback[type].emplace_back(std::move(std::make_unique<FuncSpec<Proto>>(callback)));
    }

private:
    std::map<uint8_t, std::list<std::unique_ptr<Func>>> _callback;
    std::map<uint8_t, std::function<std::shared_ptr<google::protobuf::Message>()>> _messageFactory;
};

class AbilityClient : public Ability {
public:
    class Func {
    public:
        Func() = default;
        virtual ~Func() = default;
        virtual void call(std::shared_ptr<google::protobuf::Message>) = 0;
    };

    template <typename Proto>
    class FuncSpec : public Func {
    public:
        FuncSpec(const std::function<void(std::shared_ptr<Proto>)> &call)
            : _call(call)
        {
        }

        void call(std::shared_ptr<google::protobuf::Message> message) override
        {
            _call(std::static_pointer_cast<Proto>(message));
        }

    private:
        std::function<void(std::shared_ptr<Proto>)> _call;
    };

public:
    AbilityClient();

    std::shared_ptr<google::protobuf::Message> make(std::uint8_t type) override;
    void call(std::uint8_t type,
        std::shared_ptr<Session> session,
        std::shared_ptr<google::protobuf::Message> &message) override;

    template <typename Proto>
    void addCallback(const std::function<void(std::shared_ptr<Proto>)> &callback)
    {
        const std::string typeId = Proto::GetDescriptor()->full_name();

        const std::uint8_t type = findValue(typeId);
        _messageFactory[type] = []() { return std::make_shared<Proto>(); };
        _callback[type].emplace_back(std::move(std::make_unique<FuncSpec<Proto>>(callback)));
    }

private:
    std::map<uint8_t, std::list<std::unique_ptr<Func>>> _callback;
    std::map<uint8_t, std::function<std::shared_ptr<google::protobuf::Message>()>> _messageFactory;
};

} // namespace io
