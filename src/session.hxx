#pragma once

#include "message.hxx"
#include <boost/asio.hpp>

#include <queue>

#include <memory>

class Room;
namespace io {

class Session : public std::enable_shared_from_this<Session> {
public:
    Session(boost::asio::ip::tcp::socket &&socket, std::shared_ptr<Ability> ability);
    ~Session();

    void start();
    void stop();

    void deliver(std::shared_ptr<google::protobuf::Message> &&msg);

    void onStop(std::function<void()> callback);

protected:
    void do_read_header();

    void do_read_body();

    void do_write();

private:
    boost::asio::ip::tcp::socket _socket;
    std::queue<std::shared_ptr<google::protobuf::Message>> _writeMsg;

    Message _message;
    Message _messageWrite;

    std::function<void()> _disconnectCallback;
};

} // namespace io
