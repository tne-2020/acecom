#pragma once

#ifdef LOGGING
#include <spdlog/spdlog.h>
#endif

namespace logging {

template <typename... Args>
constexpr void trace([[maybe_unused]] const char *s, [[maybe_unused]] Args... args)
{
#ifdef LOGGING
    spdlog::trace(s, args...);
#endif
}

template <typename... Args>
constexpr void debug([[maybe_unused]] const char *s, [[maybe_unused]] Args... args)
{
#ifdef LOGGING
    spdlog::debug(s, args...);
#endif
}

template <typename... Args>
constexpr void info([[maybe_unused]] const char *s, [[maybe_unused]] Args... args)
{
#ifdef LOGGING
    spdlog::info(s, args...);
#endif
}

template <typename... Args>
constexpr void warn([[maybe_unused]] const char *s, [[maybe_unused]] Args... args)
{
#ifdef LOGGING
    spdlog::warn(s, args...);
#endif
}

template <typename... Args>
constexpr void error([[maybe_unused]] const char *s, [[maybe_unused]] Args... args)
{
#ifdef LOGGING
    spdlog::error(s, args...);
#endif
}

template <typename... Args>
constexpr void critical([[maybe_unused]] const char *s, [[maybe_unused]] Args... args)
{
#ifdef LOGGING
    spdlog::critical(s, args...);
#endif
}

} // namespace logging
