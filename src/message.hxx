#pragma once

#include <array>
#include <boost/asio/buffer.hpp>
#include <cassert>
#include <cstdio>
#include <functional>
#include <list>
#include <map>
#include <memory>
#include <string>
#include <vector>

namespace google::protobuf {
class Message;
}
namespace io {
class Ability;
class Session;
} // namespace io

// A generic function to show contents of a container holding byte data
// as a string with hex representation for each byte.
//
template <class CharContainer>
std::string show_hex(const CharContainer &c)
{
    std::string hex;
    char buf[16];
    typename CharContainer::const_iterator i;
    for (i = c.begin(); i != c.end(); ++i) {
        std::sprintf(buf, "%02X ", static_cast<unsigned>(*i) & 0xFF);
        hex += buf;
    }
    return hex;
}
template <typename CharContainer>
std::string show_hex(typename CharContainer::const_iterator b,
    typename CharContainer::const_iterator e)
{
    std::string hex;
    char buf[16];
    typename CharContainer::const_iterator i;
    for (i = b; i != e; ++i) {
        std::sprintf(buf, "%02X ", static_cast<unsigned>(*i) & 0xFF);
        hex += buf;
    }
    return hex;
}

namespace io {

class Message {
    static constexpr int HEADER_SIZE_CONTENT = 2;
    static constexpr int HEADER_SIZE_TYPE = 1;
    static constexpr int HEADER_SIZE = HEADER_SIZE_CONTENT + HEADER_SIZE_TYPE;

    static constexpr int BUFFER_SIZE = 2 << 8 << HEADER_SIZE_CONTENT;
    using Buffer = std::array<uint8_t, HEADER_SIZE + BUFFER_SIZE>;

public:
    Message();
    Message(std::shared_ptr<Ability> ability);

    // Header
    uint8_t *headerBuffer();

    static constexpr int headerSize() { return HEADER_SIZE; }

    // Content
    uint8_t *contentBuffer();

    std::size_t contentSize();

    // Pack the message into the given data_buffer. The buffer is resized to
    // exactly fit the message.
    // Return false in case of an error, true if successful.
    bool pack(std::shared_ptr<google::protobuf::Message> message);

    // Unpack and store a message from the given packed buffer.
    // Return true if unpacking successful, false otherwise.
    bool unpack(const std::shared_ptr<Session> session);

protected:
    // Given a buffer with the first HEADER_SIZE bytes representing the header,
    // decode the header and return the message length. Return 0 in case of
    // an error.
    void decodeHeader();

    // Encodes the side into a header at the beginning of buf
    void encodeHeader();

private:
    int _size;
    std::uint8_t _type;

    Buffer _buffer;

    std::shared_ptr<Ability> _ability;
};

} // namespace io
