#include "client.hxx"

#include "log.hxx"
#include "session.hxx"
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <google/protobuf/message.h>

using boost::asio::ip::tcp;

namespace io {

Client::Client()
    : _ioContext()
    , _resolver(_ioContext)
    , _socket(_ioContext)
    , _ability(std::make_shared<io::AbilityClient>())
    , _stop(false)
    , _timerReconnect(_ioContext, boost::posix_time::seconds(5))
{
}

Client::~Client()
{
    stop();

    if (_thread && _thread->joinable()) {
        _thread->join();
    }
}

void Client::start(const std::string &ip, std::int16_t port)
{
    logging::warn("Client trying to connect {}:{}", ip, port);

    auto endpoints = _resolver.resolve(ip, std::to_string(port));

    if (_thread && _thread->joinable()) {
        stop(true);
        _socket = tcp::socket(_ioContext);
    }

    doConnect(endpoints);

    if (!_thread) {
        _thread = std::make_unique<std::thread>([this]() { _ioContext.run(); });
    }
}

void Client::stop(bool reconnect)
{
    _stop = !reconnect;
    if (_session) {
        _session->stop();
        _session.reset();
    }
}

void Client::onConnect() { }

void Client::onDisconnect() { }

void Client::deliver(std::shared_ptr<google::protobuf::Message> &&message)
{
    if (_session) {
        logging::trace("Sending message {}", message->GetDescriptor()->full_name());
        _session->deliver(std::move(message));
    }
    else {
        logging::trace("Adding to queue message {}", message->GetDescriptor()->full_name());
        _toBeDelivered.emplace(std::move(message));
    }
}

void Client::doConnect(const tcp::resolver::results_type &endpoints)
{
    boost::asio::async_connect(_socket,
        endpoints,
        [this, endpoints](boost::system::error_code ec, tcp::endpoint) {
            if (!ec) {
                _session = std::make_shared<Session>(std::move(_socket), _ability);
                _session->onStop(std::bind(&Client::doDisconnect, this, endpoints));

                _session->start();

                onConnect();

                while (!_toBeDelivered.empty()) {
                    deliver(std::move(_toBeDelivered.front()));
                    _toBeDelivered.pop();
                }
            }
            else {
                doDisconnect(endpoints);
            }
        });
}

void Client::doDisconnect(const tcp::resolver::results_type &endpoints)
{
    if (!_stop) {
        logging::warn("Client losing connexion, trying to reconnect");

        _timerReconnect.expires_from_now(boost::posix_time::seconds(5));
        _timerReconnect.async_wait(boost::bind(&Client::start,
            this,
            endpoints->host_name(),
            std::atoi(endpoints->service_name().c_str())));
    }
    else {
        logging::info("Client successful disconnected");
    }

    onDisconnect();
}

} // namespace io
