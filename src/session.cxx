#include "session.hxx"

#include "log.hxx"
#include <boost/bind.hpp>
#include <google/protobuf/message.h>

#include <iostream>

using boost::asio::ip::tcp;

namespace io {

Session::Session(tcp::socket &&socket, std::shared_ptr<io::Ability> ability)
    : _socket(std::move(socket))
    , _message(ability)
{
}

Session::~Session()
{
    logging::trace(__PRETTY_FUNCTION__);
}

void Session::start()
{
    do_read_header();
}

void Session::stop()
{
    if (_socket.is_open()) {
        _socket.close();
    }
}

void Session::deliver(std::shared_ptr<google::protobuf::Message> &&msg)
{
    if (msg) {
        logging::trace("Session ({}): Deliver {}\n{}",
            (long long)this,
            msg->GetDescriptor()->full_name(),
            msg->DebugString());
    }

    bool write_in_progress = !_writeMsg.empty();
    _writeMsg.push(std::move(msg));
    if (!write_in_progress) {
        do_write();
    }
}

void Session::onStop(std::function<void()> callback)
{
    _disconnectCallback = callback;
}

void Session::do_read_header()
{
    auto self(shared_from_this());

    boost::asio::async_read(_socket,
        boost::asio::buffer(_message.headerBuffer(), io::Message::headerSize()),
        [this, self](boost::system::error_code ec, std::size_t /*length*/) {
            if (!ec) {
                do_read_body();
            }
            else if ((boost::asio::error::eof == ec)
                || (boost::asio::error::connection_reset == ec)) {
                logging::critical("Disconnected {}", ec.message());

                if (_disconnectCallback) {
                    _disconnectCallback();
                }
            }
            else {
                logging::critical("Read header error : ({}){}", ec.value(), ec.message());
            }
        });
}

void Session::do_read_body()
{
    const std::size_t size = _message.contentSize();

    boost::asio::async_read(_socket,
        boost::asio::buffer(_message.contentBuffer(), size),
        [this, self = shared_from_this(), size](boost::system::error_code ec, std::size_t length) {
            if (!ec && size == length) {
                _message.unpack(self);

                do_read_header();
            }
            else if (ec) {
                logging::critical("Read body error : ({}){}", ec.value(), ec.message());
            }
        });
}

void Session::do_write()
{
    auto self(shared_from_this());

    if (_messageWrite.pack(_writeMsg.front())) {
        const std::size_t size = _messageWrite.headerSize() + _messageWrite.contentSize();
        boost::asio::async_write(_socket,
            boost::asio::buffer(_messageWrite.headerBuffer(), size),
            [this, self](boost::system::error_code ec, std::size_t /*length*/) {
                if (!ec) {
                    _writeMsg.pop();
                    if (!_writeMsg.empty()) {
                        do_write();
                    }
                }
                else {
                    logging::critical("Write error : ({}){}", ec.value(), ec.message());
                }
            });
    }
    else {
        _writeMsg.pop();
        if (!_writeMsg.empty()) {
            do_write();
        }
    }
}

} // namespace io
