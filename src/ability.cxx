#include "ability.hxx"

namespace io {

AbilityServer::AbilityServer() { }

std::shared_ptr<google::protobuf::Message> AbilityServer::make(uint8_t type)
{
    if (auto found = _messageFactory.find(type); found != _messageFactory.end()) {
        return found->second();
    }

    return {};
}

void AbilityServer::call(uint8_t type,
    std::shared_ptr<Session> session,
    std::shared_ptr<google::protobuf::Message> &message)
{
    if (auto found = _callback.find(type); found != _callback.end()) {
        for (auto &callback : found->second) {
            callback->call(session, message);
        }
    }
}

AbilityClient::AbilityClient() { }

std::shared_ptr<google::protobuf::Message> AbilityClient::make(uint8_t type)
{
    if (auto found = _messageFactory.find(type); found != _messageFactory.end()) {
        return found->second();
    }

    return {};
}

void AbilityClient::call(uint8_t type,
    std::shared_ptr<Session> /* session */,
    std::shared_ptr<google::protobuf::Message> &message)
{
    if (auto found = _callback.find(type); found != _callback.end()) {
        for (auto &callback : found->second) {
            callback->call(message);
        }
    }
}

} // namespace io
